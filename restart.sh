#!/bin/bash
docker ps --last -1 --format "{{.ID}}"| xargs docker stop
./mvnw clean
./mvnw package

docker run -p 8080:8080 -t reportService/reportService