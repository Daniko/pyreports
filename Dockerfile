FROM openjdk:8
VOLUME /tmp
#directory = /home/dan/IdeaProjects/report
ADD target/reportService-0.0.1.jar app.jar
COPY src/main/resources/report /report
CMD ["java","-Djava.security.egd=file:/dev/./urandom","-jar","app.jar"]