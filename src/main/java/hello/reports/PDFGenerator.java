package hello.reports;

import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.*;
import com.itextpdf.layout.property.TabAlignment;
import com.itextpdf.layout.property.TextAlignment;
import hello.storage.StorageException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@Component
@Scope("prototype")
public class PDFGenerator {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private PDFGeneratorProperties properties;

    @Autowired
    public PDFGenerator(PDFGeneratorProperties properties) {
        this.properties = properties;
    }

    public String createDocument(ReportService report) {

        String reportName = "lab" + report.getLabNumber() + ".pdf";


        try (FileOutputStream fos = new FileOutputStream(report.getPythonProjectPath().resolve(reportName).toString());
             PdfWriter writer = new PdfWriter(fos);
             PdfDocument pdfDocument = new PdfDocument(writer);
             Document document = new Document(pdfDocument);) {

             PdfFont textFont = PdfFontFactory.createFont(properties.getStandardFont(), "Identity-H", true);

            ImageData data = ImageDataFactory.create(properties.getBmstuImgPath());
            Image image = new Image(data);
            image.setMarginTop(10);
            image.setBackgroundColor(Color.WHITE);
            document.add(image);

            Paragraph mainTitle = new Paragraph(properties.getBmstuTitle() + "\n" + report.getLabName());
            mainTitle.setFont(textFont);
            mainTitle.setTextAlignment(TextAlignment.CENTER);
            document.add(mainTitle);


            Paragraph studentName = new Paragraph("Выполнил: " + report.getStudentName() + "\n" + "Проверил: Гапанюк Ю.Е.");
            studentName.setFont(textFont);
            studentName.setFontSize(10);
            studentName.setTextAlignment(TextAlignment.RIGHT);
            studentName.setMarginTop(350);
            document.add(studentName);

            Paragraph footer = new Paragraph("Москва - " + LocalDate.now().getYear());
            footer.setTextAlignment(TextAlignment.CENTER);
            footer.setMarginTop(65);
            footer.setFont(textFont);
            footer.setFontSize(10);
            document.add(footer);

            pdfDocument.addNewPage();

            Paragraph description = new Paragraph("Цель: " + report.getLabPurpose());
            description.setFont(textFont);
            description.setMarginTop(5);
            document.add(description);

            for (Map.Entry<String, List<String>> entry : report.getPythonFilesContent().entrySet()) {
                AreaBreak areaBreak = new AreaBreak();
                document.add(areaBreak);


                Paragraph filename = new Paragraph(entry.getKey());
                filename.setFont(textFont);
                filename.setMarginTop(5);
                filename.setTextAlignment(TextAlignment.CENTER);
                filename.setBold();
                document.add(filename);


                Paragraph code = new Paragraph();
                code.setFont(textFont);
                code.setFontSize(6);
                code.setMarginTop(10);

                List<String> lines = entry.getValue();
                String prevLine="";
                code.addTabStops(new TabStop(5f, TabAlignment.LEFT));

                for (int i = 0; i <lines.size() ; i++) {
                    String line = lines.get(i);
                    if (prevLine.endsWith(":")&&!prevLine.startsWith("#")) {
                     code.add(new Tab());
                    }else{
                    }
                    code.add(line+"\n");
                    prevLine = line;
                }

                document.add(code);
            }



            document.close();
        } catch (IOException exception) {
            throw new StorageException("cause ", exception);
        } finally {
           // document.close(); WANT JAVA 9 EVERYWHERE
        }
        return reportName;
    }

}

