package hello.reports;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;

@Component
@ConfigurationProperties
public class PDFGeneratorProperties {

     private int smallFontSize = 16;
     private int bigFontSize = 32;

    public int getSmallFontSize() {
        return smallFontSize;
    }

    public void setSmallFontSize(int smallFontSize) {
        this.smallFontSize = smallFontSize;
    }

    public int getBigFontSize() {
        return bigFontSize;
    }

    public void setBigFontSize(int bigFontSize) {
        this.bigFontSize = bigFontSize;
    }

    public int getVeryBigFontSize() {
        return veryBigFontSize;
    }

    public void setVeryBigFontSize(int veryBigFontSize) {
        this.veryBigFontSize = veryBigFontSize;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public String getBmstuImgPath() {
        return bmstuImgPath;
    }

    public void setBmstuImgPath(String bmstuImgPath) {
        this.bmstuImgPath = bmstuImgPath;
    }

    public String getBmstuTitle() {
        return bmstuTitle;
    }

    public String getStandardFont() {
        return standardFont;
    }

    private int veryBigFontSize = 200;

    private int offset = 40;

    //private String bmstuImgPath =Paths.get("/home/dan/IdeaProjects/report/src/main/resources/report/mgtu.png").toString();
    private String bmstuImgPath =Paths.get(System.getProperty("user.dir"), "report" ,"bmstu.png").toString();

    private final String bmstuTitle = "Федеральное государственное бюджетное образовательное учреждение высшего профессионального образования\n" +
            "МОСКОВСКИЙ ГОСУДАРСТВЕННЫЙ ТЕХНИЧЕСКИЙ УНИВЕРСИТЕТ им. Н. Э. БАУМАНА" + "\n" + "\n" +
            "Лабораторная работа по курсу:\n" +
            "«Разработка Интернет Приложений»\n";

    //private final String standardFont = Paths.get("/home/dan/IdeaProjects/report/src/main/resources/report/FreeSans.ttf").toString();
    private final String standardFont = Paths.get(System.getProperty("user.dir"), "report" ,"FreeSans.ttf").toString();
}
