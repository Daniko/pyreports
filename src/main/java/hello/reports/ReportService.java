package hello.reports;

import hello.storage.StorageException;
import hello.storage.StorageProperties;
import org.apache.commons.io.FileUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.logging.Logger;

@Component
@Scope("prototype")
public class ReportService {

    private String labName;

    private String studentName;

    private Path pythonProjectPath;

    private int labNumber;

    private String labPurpose;


    private HashMap<String, List<String>> pythonFilesContent;

    private final PDFGenerator pdfGenerator;

    private final org.slf4j.Logger logger= LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ReportService(StorageProperties storageProperties, PDFGenerator pdfGenerator){
        this.pdfGenerator = pdfGenerator;
    }

    public String generateReportFile(String labName, String studentName, int labNumber, String labPurpose, Path pythonProjectPath){
        this.labName = labName;
        this.studentName = studentName;
        this.labNumber = labNumber;
        this.labPurpose = labPurpose;
        this.pythonProjectPath = pythonProjectPath;
        pythonFilesContent = new HashMap<>();

        return processFiles();
    }

    private boolean findFiles(){
        boolean isFind =false;
        Iterator iterator = FileUtils.iterateFiles(pythonProjectPath.toFile(), new String[]{"py"}, true);

        if(iterator.hasNext()) {
            isFind = true;
            while (iterator.hasNext()) {
                File pythonCodeFile = (File) iterator.next();
                try{
                    List<String>content = Files.readAllLines(pythonCodeFile.toPath());
                    pythonFilesContent.put(pythonCodeFile.getName(), content);
                } catch (IOException e) {
                    logger.warn("Errors while reading file: "+pythonCodeFile.getName() ,e);
                }
            }
        }

        return isFind;
    }


    public String processFiles(){
        if(pythonProjectPath == null){
            String errorMessage = "Project file is not found: "+ pythonProjectPath.toString();
            logger.error(errorMessage);
            throw  new StorageException(errorMessage);
        }
        else if (!findFiles()){
            String errorMessage = ".py files not found in file: " + pythonProjectPath.toString();
            logger.error(errorMessage);
            throw new StorageException(errorMessage);
        }else {
            String filename = pdfGenerator.createDocument(this);
            logger.info("document "+ filename +" was created");
            return filename;
        }
    }

    public String getLabName(){
        return labName;
    }

    public String getStudentName(){
        return studentName;
    }

    public int getLabNumber(){
        return labNumber;
    }

    public String getLabPurpose(){
        return labPurpose;
    }

    public HashMap<String, List<String>> getPythonFilesContent() {
        return pythonFilesContent;
    }

    public Path getPythonProjectPath() {
        return pythonProjectPath;
    }


}