package hello.storage;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PreDestroy;

@Service
@Scope("prototype")
public class FileSystemStorageService implements StorageService {

    private  Path rootLocation;

    private Path userDirectory;

    @Autowired
    public FileSystemStorageService(StorageProperties properties) {
        this.rootLocation = Paths.get(properties.getLocation());
        this.userDirectory = rootLocation;
    }

    @Override
    public Path store(MultipartFile[] files, String userId) {
        try {
        this.userDirectory =Files.createDirectories(this.rootLocation.resolve(userId));

        }catch (IOException e){
            throw new StorageException("Failed to create directory"+ userDirectory);
        }
        for (MultipartFile file:
             files) {
            Path path = Paths.get(file.getOriginalFilename());
            String filename = path.getFileName().toString();

            try {
                if (file.isEmpty()) {
                    throw new StorageException("Failed to store empty file " + filename);
                }
                if (filename.contains("..")) {
                    // This is a security check
                    throw new StorageException(
                            "Cannot store file with relative path outside current directory "
                                    + filename);
                }
                Files.copy(file.getInputStream(), userDirectory.resolve(filename),
                        StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                throw new StorageException("Failed to store file " + filename, e);
            }
        }
        return userDirectory;
    }

    @Override
    public Stream<Path> loadAll() {
        try {
            return Files.walk(this.userDirectory, 1)
                    .filter(path -> !path.equals(this.userDirectory))
                    .map(path -> this.userDirectory.relativize(path));
        }
        catch (IOException e) {
            throw new StorageException("Failed to read stored files", e);
        }

    }

    @Override
    public Path load(String filename) {
        return userDirectory.resolve(filename);
    }

    @Override
    public Resource loadAsResource(String filename) {
        try {
            Path file = load(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            }
            else {
                throw new StorageFileNotFoundException(
                        "Could not read file: " + filename);

            }
        }
        catch (MalformedURLException e) {
            throw new StorageFileNotFoundException("Could not read file: " + filename, e);
        }
    }

    @Override
    @PreDestroy
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(userDirectory.toFile());
    }

    @Override
    public void init() {
        try {
            Files.createDirectories(rootLocation);
        }
        catch (IOException e) {
            throw new StorageException("Could not initialize storage", e);
        }
    }
}
