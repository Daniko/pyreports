package hello;

import java.io.IOException;
import java.nio.file.Path;

import hello.reports.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import hello.storage.StorageFileNotFoundException;
import hello.storage.StorageService;

import javax.servlet.http.HttpSession;

@Controller
@Scope("session")
public class FileUploadController {

    private final StorageService storageService;

    private final ReportService reportService;

    private String filename ;


    @Autowired
    public FileUploadController(StorageService storageService, ReportService reportService) {
        this.storageService = storageService;
        this.reportService = reportService;
    }

    @PostMapping("/")
    public String handleFileUpload(@RequestParam("files") MultipartFile files[], @RequestParam("fio") String fio, @RequestParam("labname") String labname,
                                   @RequestParam("purpose") String purpose, @RequestParam("labnum") int num, RedirectAttributes redirectAttributes, HttpSession session) {

        String userId = session.getId().substring(1,5);
        Path savedFiles=  storageService.store(files,userId);
        filename = reportService.generateReportFile(labname,fio,num,purpose,savedFiles);

        redirectAttributes.addFlashAttribute("message", "You successfully uploaded " + files[0].getOriginalFilename().split("/")[0] + "!");
        return "redirect:/result";
    }


    @GetMapping("/result")
    public String listUploadedFiles(Model model) throws IOException {

        model.addAttribute("file",  MvcUriComponentsBuilder.fromMethodName(FileUploadController.class,
                "serveFile", filename).build().toString());

        return "getpdf";
    }

    @GetMapping("/files/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

        Resource file = storageService.loadAsResource(filename);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }


    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }

    @GetMapping("/")
    public String handleWelcome(){
        storageService.deleteAll();
        return "uploadfile";
    }

//    @GetMapping("/{filename}")
//    public String handleWelcome(@PathVariable String filename, Model model){
//        Path dir = Paths.get(filename);
//        StringBuilder builder = new StringBuilder();
//        try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir)) {
//            for (Path file: stream) {
//                builder.append(file.getFileName()).append("\n");
//            }
//        } catch (IOException | DirectoryIteratorException x) {
//            // IOException не может броситься во время итерации.
//            // В этом куске кода оно может броситься только
//            // методом newDirectoryStream.
//            System.err.println(x);
//        }
//        model.addAttribute("message",builder.toString() );
//        return "debug";
//    }
}
